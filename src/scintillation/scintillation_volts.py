#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  NOISE
#
#    Steven Hale
#    2017 September 3
#    Tenerife
#

import sys
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy

import core.plot as plot
import core.fft as fft
import core.keysight as keysight

########################################################################
########################################################################
########################################################################

def calcScintillation(sFilespec):

    #### Load atmospheric noise

    aNoise = keysight.read(sFilespec)

    dt = aNoise['time'][1]-aNoise['time'][0]
    print('Reading file: {}'.format(sFilespec))
    print()
    print('Time domain:')
    print('         dt: {:6.3f} mS'.format(1000*dt))
    print('     energy: {:6.3f}'.format(fft.calc_energy_in_time_domain(aNoise['ch1'], dt)))
    print('        max: {:6.3f} V'.format(numpy.max(aNoise['ch1'])))
    print('        min: {:6.3f} V'.format(numpy.min(aNoise['ch1'])))
    print('       mean: {:6.3f} V'.format(numpy.mean(aNoise['ch1'])))
    print('        std: {:6.3f} mV'.format(1000*numpy.std(aNoise['ch1'])))
    print()

    #responsivity = 0.45 # amps per watt
    #current = self.responsivity * self.I # amps
    #print('Transmission current: {:.4f} uA'.format(current*1e6))
    #gain = 30e3 # 30k gain resistor
    #voltage = current * gain # v=ir

    #### Convert to spectral density

    #power=False
    power=True
    parseval=True

    aNoise['ch1'] -= numpy.mean(aNoise['ch1'])
    aSD = fft.calc_fft(aNoise['ch1'], dt, power=power, parseval=parseval)

    print('Frequency domain:')
    print('   RMS: {:6.3f} mV'.format(1000*fft.spectrum_to_rms(aSD['spectrum'], dt, power=power)))
    print()

    return aSD

def main(date):

    data_dir = '../../data'

    plot.set_latex()
    plot.set_fonts()

    # Create a canvas.
    fig = plot.canvas('normal')
    ax1 = fig.add_subplot(111)
    plot.confax(ax1)

    # Set up the axes.
    ax1.set_xlabel('Frequency (Hz)')
    ax1.set_ylabel('Amplitude Spectral Density ($\mu\mathrm{V}/\sqrt{\mathrm{Hz}}$)')

    aFiles = glob.glob('{}/{}/*.csv'.format(data_dir, date))

    for sFilespec in aFiles:

        aThisSD = calcScintillation(sFilespec)

        try:
            aSD['spectrum'] += aThisSD['spectrum']
        except NameError:
            aSD = aThisSD

    aSD['spectrum'] = aSD['spectrum'] / len(aFiles)
    aSD['spectrum'] = 1e6 * numpy.sqrt(aSD['spectrum'])

    ax1.semilogx(aSD['freqs'], aSD['spectrum'], color='k', linewidth=1, solid_capstyle='butt', zorder=2)

    ax1.set_xlim(0,1e5)
    ax1.set_ylim(0,1000)

    pdfFilespec = '{}.pdf'.format(date)
    plot.save(fig, pdfFilespec)
    plt.close()

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main('20170903-morning')
    #main('../data/20170904-morning')
    #main('../data/20170905-morning')
    #main('../data/20170906-morning')
    #main('../data/20170908-morning')
    #main('../data/20170909-morning')

########################################################################
