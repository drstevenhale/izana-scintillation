#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  SCINTILLATION
#
#    Steven Hale
#    2017 September 3
#    Izana, Tenerife, Spain
#

"""Plot daily scintillation measurements at Izana."""

import sys
import glob
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy

import core.plot as plot
import core.fft as fft
import core.keysight as keysight

########################################################################
########################################################################
########################################################################

def movingmean(aData, nWindow):

    aWindow = numpy.ones(int(nWindow))/float(nWindow)
    aMM = numpy.convolve(aData, aWindow, 'same')
    return aMM

def calcScintillation(aFiles):

    for sFilespec in aFiles:

        #### Load atmospheric noise

        aNoise = keysight.read(sFilespec)

        dt = aNoise['time'][1]-aNoise['time'][0]
        #print('Reading file: {}'.format(sFilespec))
        #print()
        #print('Time domain:')
        #print('         dt: {:6.3f} mS'.format(1000*dt))
        #print('     energy: {:6.3f}'.format(fft.energyInTimeDomain(aNoise['ch1'], dt)))
        #print('        max: {:6.3f} V'.format(numpy.max(aNoise['ch1'])))
        #print('        min: {:6.3f} V'.format(numpy.min(aNoise['ch1'])))
        #print('       mean: {:6.3f} mW'.format(1e3*numpy.mean(aNoise['ch1'])/30e3/0.45))
        #print('        std: {:6.3f} mV'.format(1000*numpy.std(aNoise['ch1'])))
        #print()

        #### Convert to Watts

        responsivity = 0.45 # amps per watt
        gain = 30e3 # 30k gain resistor

        aNoise['ch1'] = aNoise['ch1'] / gain # I = V/R
        aNoise['ch1'] = aNoise['ch1'] / responsivity # Amps to Watts

        #### Convert to spectral density

        aThisSD = fft.calc_fft(aNoise['ch1'], dt, power=True, parseval=True, zeromean=True)

        #print('Frequency domain:')
        #print('   std: {:6.3f} mV'.format(1000*numpy.std(aNoise['ch1'])))
        #print('   RMS: {:6.3f} mV'.format(1000*fft.spectrum_to_rms(aThisSD['spectrum'], dt, power=True)))
        #print()

        try:
            aSD['spectrum'] += aThisSD['spectrum']
        except NameError:
            aSD = aThisSD

    scale = 1e9

    aSD['spectrum'] = aSD['spectrum'] / len(aFiles)
    aSD['spectrum'] = scale * numpy.sqrt(aSD['spectrum'])

    return aSD

def main():

    plot.set_latex()
    plot.set_fonts()

    aDays = ['20170903-morning', '20170904-morning', '20170905-morning',
             '20170906-morning', '20170908-morning', '20170909-morning',
             '20170910-morning']

    aTitles = ['September 3', 'September 4', 'September 5',
               'September 6', 'September 8', 'September 9',
               'September 10']

    #aDust = [0.0017, 0.0150, 0.0080, 0.0009, 0.0025, 0.0015, 0.0017]
    aDust = ['Little', 'Quite a lot', 'Medium', 'Little', 'Medium', 'Little', 'Little']

    data_dir = '../data'

    nDays = len(aDays)
    nRows = 4
    nCols = 2

    subplotSpec = int('{}{}1'.format(nRows, nCols))

    # Create a canvas.
    fig = plot.canvas('full')
    for i in range(subplotSpec, subplotSpec+nDays):
        fig.add_subplot(i)

    for i, ax in enumerate(fig.axes):
        print(aDays[i])
        plot.confax(ax)
        ax.set_title(aTitles[i])
        ax.set_xlabel('Frequency (Hz)')
        ax.set_ylabel('ASD ($\mathrm{nW}/\sqrt{\mathrm{Hz}}$)')

        ax.set_xlim(1e-1, 3.125e3)
        ax.set_ylim(0,40)

        aFiles = glob.glob('{}/{}/*.csv'.format(data_dir, aDays[i]))

        aSD = calcScintillation(aFiles)
        ax.semilogx(aSD['freqs'], aSD['spectrum'], color='k', linewidth=1, solid_capstyle='butt', zorder=1)

        df = aSD['freqs'][1] - aSD['freqs'][0]
        nWindow = 1 # Hz
        nWindow = nWindow / df

        aSD['spectrum'] = movingmean(aSD['spectrum'], nWindow)

        whiteStart = 5
        whiteStop  = 10
        whiteMean = numpy.mean(aSD['spectrum'][whiteStart:whiteStop])
        print('mean: ', whiteMean, 'over: ', aSD['freqs'][whiteStart], aSD['freqs'][whiteStop])
        dB3  = whiteMean * numpy.sqrt(0.5)   # 0.707
        dB6  = whiteMean * numpy.sqrt(0.25)  # 0.5
        dB9  = whiteMean * numpy.sqrt(0.125) # 0.3536
        dB10 = whiteMean * numpy.sqrt(0.1)   # 0.3162

        dB3freq  = aSD['freqs'][numpy.argmax(aSD['spectrum'][whiteStart:]<dB3)]
        dB6freq  = aSD['freqs'][numpy.argmax(aSD['spectrum'][whiteStart:]<dB6)]
        dB9freq  = aSD['freqs'][numpy.argmax(aSD['spectrum'][whiteStart:]<dB9)]
        dB10freq = aSD['freqs'][numpy.argmax(aSD['spectrum'][whiteStart:]<dB10)]

        print('   -3dB: {}'.format(dB3freq))
        print('   -6dB: {}'.format(dB6freq))
        print('   -9dB: {}'.format(dB9freq))
        print('  -10dB: {}'.format(dB10freq))

        guideLines = False
        if guideLines:

            ax.semilogx(aSD['freqs'], aSD['spectrum'], color='r', linewidth=1, solid_capstyle='butt', zorder=2)

            ax.axvline(x=aSD['freqs'][whiteStart])
            ax.axvline(x=aSD['freqs'][whiteStop])
            ax.axhline(y=whiteMean)

            ax.axhline(y=dB6, color='g')
            ax.axvline(x=dB6freq, color='g')

        ax.text(0.95, 0.90, 'Dust: {}'.format(aDust[i]), color='k', horizontalalignment='right', transform=ax.transAxes)

        ax.text(0.75, 0.75, '-3dB:', color='k', horizontalalignment='right', transform=ax.transAxes)
        ax.text(0.95, 0.75, '{:~>2.0f}\,Hz'.format(dB3freq), color='k', horizontalalignment='right', transform=ax.transAxes)

        ax.text(0.75, 0.60, '-6dB:', color='k', horizontalalignment='right', transform=ax.transAxes)
        ax.text(0.95, 0.60, '{:~>2.0f}\,Hz'.format(dB6freq), color='k', horizontalalignment='right', transform=ax.transAxes)

        ax.text(0.75, 0.45, '-9dB:', color='k', horizontalalignment='right', transform=ax.transAxes)
        ax.text(0.95, 0.45, '{:~>2.0f}\,Hz'.format(dB9freq), color='k', horizontalalignment='right', transform=ax.transAxes)

        ax.text(0.75, 0.30, '-10dB:', color='k', horizontalalignment='right', transform=ax.transAxes)
        ax.text(0.95, 0.30, '{:~>2.0f}\,Hz'.format(dB10freq), color='k', horizontalalignment='right', transform=ax.transAxes)

        locmaj = mpl.ticker.LogLocator(base=10,numticks=6)
        ax.xaxis.set_major_locator(locmaj)

        locmin = mpl.ticker.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=12)
        ax.xaxis.set_minor_locator(locmin)
        ax.xaxis.set_minor_formatter(mpl.ticker.NullFormatter())

    plot.save(fig, '../paper/izana_scintillation.pdf')
    plt.close()

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main()

########################################################################
