#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  FFT.PY
#
#    Steven Hale
#    2016 July 19
#    Birmingham, UK
#

"""Returns a correctly scaled spectral density array."""

import numpy

########################################################################
########################################################################
########################################################################

def generate_signal(amplitude, mean, frequency, rate, duration):

    """Return a sine wave of the requested amplitude and frequency, for
    the specified duration, at the requested sampling rate.

    Parameters
    ----------
    :param amplitude: The sine wave amplitude.
    :type amplitude: float

    :param mean: Mean ("DC offset") of the signal.
    :type mean: float

    :param frequency: The sine wave frequency in Hz.
    :type frequency: float

    :param rate: The sampling rate in Hz.
    :type rate: float

    :param duration: The duration of the signal in seconds.
    :type duration: float

    Returns
    -------
    :returns: Dictionary of ['time'] and ['signal'] values.
    :rtype: array of floats
    """

    array_time = numpy.arange(duration * rate) / rate
    array_signal = amplitude * numpy.sin(2 * numpy.pi * frequency * array_time)
    array_signal += mean
    return {'time':array_time, 'signal':array_signal}

########################################################################

def generate_random_noise(amplitude, mean, points):

    """
    Return random floats from the "continuous uniform" distribution
    using :func:`numpy.random.random` with the specified amplitude and
    mean.

    Parameters
    ----------
    :param amplitude: Values are returned between +-amplitude.
    :type amplitude: float

    :param mean: Mean ("centre") of the distribution.
    :type mean: float

    :param points: Number of samples drawn from the distribution.
    :type points: int

    Returns
    -------
    :returns: Drawn samples from the "continuous uniform" distribution.
    :rtype: float or array of floats

    """

    array_noise = numpy.random.random(points)
    # Convert from [0,1] to [-1,1]
    array_noise -= numpy.mean(array_noise)
    array_noise = 2 * array_noise
    # Scale by requested amplitude
    array_noise = amplitude * array_noise
    # Offset by requested mean
    array_noise += mean

    return array_noise

########################################################################

def generate_gaussian_noise(sigma, mean, points):

    """
    Return random floats from the parameterized normal distribution
    using :func:`numpy.random.normal` with the specified sigma and
    mean.

    Parameters
    ----------
    :param sigma: Standard deviation (spread or "width") of the distribution.
    :type sigma: float

    :param mean: Mean ("centre") of the distribution.
    :type mean: float

    :param points: Number of samples drawn from the distribution.
    :type points: int

    Returns
    -------
    :returns: Drawn samples from the parameterized normal distribution.
    :rtype: float or array of floats

    """

    return numpy.random.normal(loc=mean, scale=sigma, size=points)

########################################################################

def calc_energy_in_time_domain(data, dt):

    """
    The energy in the time domain is equal to the amplituded
    squared, integrated over the time interval.

    If the spectrum is correctly scaled according to Parseval's
    theorem then this should be equal to the energy in the frequency
    domain calculated by :func:`calc_energy_in_frequency_domain`.
    """

    return numpy.sum(data**2 * dt)

def calc_energy_in_frequency_domain(data, dt):

    """
    The energy In the frequency domain is equal to the magnitude of
    the amplitude squared, integrated over the frequency interval.

    If the spectrum is correctly scaled according to Parseval's
    theorem then this should be equal to the energy in the time
    domain calculated by :func:`calc_energy_in_time_domain`.
    """

    sd = calc_fft(data, dt, power=True, parseval=True)
    return numpy.sum(sd['spectrum'])

########################################################################

def calc_fft(data, dt, power=True, parseval=True, zeromean=False):

    """Calculate the spectral density, scaled according to the given options."""

    nonZero = numpy.count_nonzero(data)
    fill = nonZero / data.size

    if zeromean:
        data -= numpy.mean(data)

    aFFT = numpy.fft.fft(data)
    aFFT = aFFT / aFFT.size
    aMagnitude = numpy.abs(aFFT)
    aPhase     = numpy.angle(aFFT)

    # Convert from double-sided to single-sided.  Take the real half
    # and multiply by sqrt(2) to compensate for the lost amplitude (a
    # doubling of power).
    aSpectrum = numpy.sqrt(2) * aMagnitude[0:aFFT.size//2]
    aPhase    = aPhase[0:aFFT.size//2]

    # Compensate for the fill.
    aSpectrum = aSpectrum / numpy.sqrt(fill)

    if parseval:
        df = 1.0 / (data.size * dt)
        aSpectrum = aSpectrum / numpy.sqrt(df)

    if power:
        aSpectrum = aSpectrum**2

    # Calculate the frequencies.
    aFreqs = numpy.fft.fftfreq(data.size, dt)
    aFreqs = aFreqs[0:aFreqs.size//2]

    return {'freqs':aFreqs, 'spectrum':aSpectrum, 'phase':aPhase}

########################################################################

def spectrum_to_rms(aSD, dt, power=True, parseval=True):

    """Calculate the RMS of the given spectral density."""

    # We want to integrate the spectral density over the whole
    # bandwidth and return the RMS.  This is the square root of the
    # sum of the squared magnitude.
    #
    # We need to rescale the spectrum back to magnitude to do this,
    # depending on what scaling technique is currently in use.

    if not power:
        aSD = aSD**2

    if parseval:
        df = 1.0 / (2 * aSD.size * dt)
        return numpy.sqrt(numpy.sum(aSD * df))
    else:
        return numpy.sqrt(numpy.sum(aSD))

########################################################################

def spectrum_to_timeseries(aSpectrum, dt, power=True, parseval=True):

    """Produce a timeseries with the given spectral density and random phase."""

    # In order to convert from amplitude or power spectral density
    # back to a timeseries, we first need to undo all of the scaling.

    if power:
        aSpectrum = numpy.sqrt(aSpectrum)

    if parseval:
        df = 1.0 / (2 * aSpectrum.size * dt)
        aSpectrum = aSpectrum * numpy.sqrt(df)

    # We are going to convert from single-sided back to double-sided.
    # We need to divide by sqrt(2) to compensate for the amplitude (a
    # halving of power).

    aMagnitude = aSpectrum / numpy.sqrt(2)

    # The magnitude needs to be symmetric, with the negative
    # frequencies repeated at the top half of the array.
    #
    # The positive frequencies go from 0 -> x-1
    # The negative frequencies go from -x -> -1
    #
    # i.e., for even-N:  0, 1, 2, 3, 4, -5, -4, -3, -2, -1
    #       for  odd-N:  0, 1, 2, 3, 4, 5, -5, -4, -3, -2, -1
    #
    # If the FFT has an even number of points, we don't have enough
    # information to fully reconstruct the FFT ready to be inverted.
    # This only works properly for odd-N.  I'm not sure what to do in
    # the case of even-N.

    aMagnitudeTopHalf = numpy.flip(aMagnitude[1:], axis=0)
    aMagnitude = numpy.concatenate((aMagnitude, aMagnitudeTopHalf))

    # Un-normalise
    aMagnitude = aMagnitude * aMagnitude.size

    # SPICE does not generate phase information for the noise
    # simulation.  This does not matter since it is just noise and so
    # we can randomly generate phase values.  The phase needs to be
    # anti-symmetric, with the negative frequencies in the top half of
    # the array and inverted.

    N = aMagnitude.size//2
    aPhaseBottom = 2 * numpy.pi * numpy.random.randn(N+1)
    aPhaseTop    = numpy.flip(-1 * aPhaseBottom[1:], axis=0)
    aPhase = numpy.concatenate((aPhaseBottom, aPhaseTop))

    # Add the phase information by complex multiplication.
    aFFT = aMagnitude * numpy.exp(1j * aPhase)

    # Now take the inverse FFT.
    invFFT = numpy.fft.ifft(aFFT)

    # The timeseries from inverted FFT is real valued.  But there are
    # some very close to zero complex values due to numerical
    # precision.  It's ok to take only the real part and drop the
    # near-zero complex part.
    aData = invFFT.real

    # Finally generate an array of times for subsequent plotting.

    aTime = numpy.arange(0, ((2*N)+1)*dt, dt)

    return {'time':aTime, 'data':aData}

########################################################################
