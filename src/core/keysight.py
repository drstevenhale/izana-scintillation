#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  KEYSIGHT.PY
#
#    Steven Hale
#    2016 February 10
#    Birmingham, UK
#

"""This module reads .csv files from a Keysight oscilloscope."""

import numpy

########################################################################
########################################################################
########################################################################

def read(filespec, num_channels=1):

    """Build the list of channel names based on the specified number of
    channels, then read the file.

    """

    names = 'time'
    for channel in range(num_channels):
        names += ',ch{}'.format(channel+1)

    data = numpy.genfromtxt(filespec,
                            delimiter=',',
                            dtype=float,
                            skip_header=2,
                            names=names)

    return data

########################################################################
