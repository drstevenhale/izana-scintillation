#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  PLOT.PY
#
#    Steven Hale
#    2015 August 13
#    Birmingham, UK
#

"""This Python module configures some standard plot settings.  By
making use of this module we achieve standardisation across all
document figures.

"""

import matplotlib as mpl
import matplotlib.pyplot as plt

########################################################################
########################################################################
########################################################################

def set_latex():

    """Activate LaTeX for text."""

    # Use LaTeX for the labels.
    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['font.family'] = 'serif'
    # Use the LaTeX default Computer Modern fonts by clearing the list
    # for font.serif.
    #mpl.rcParams['font.serif']  = []

def set_fonts(font_size=12):

    """Set font size for all text."""

    plt.rc('font', size=font_size) # controls default text sizes
    plt.rc('axes', titlesize=font_size) # fontsize of the axes title
    plt.rc('axes', labelsize=font_size) # fontsize of the x any y labels
    plt.rc('xtick', labelsize=font_size) # fontsize of the tick labels
    plt.rc('ytick', labelsize=font_size) # fontsize of the tick labels
    plt.rc('legend', fontsize=font_size) # legend fontsize
    plt.rc('figure', titlesize=font_size) # # size of the figure title

def confax(ax1, side='left'):

    """Configure a standard figure with a left-hand axis.  If two y-axes
    are required then this function should be called twice, once for
    the left-hand axis and again with side='right' for the right-hand
    axis.

    """

    if side == 'left':
        ax1.spines["top"].set_visible(False)
        ax1.spines["right"].set_visible(False)
        ax1.xaxis.set_ticks_position('bottom')
        ax1.yaxis.set_ticks_position('left')
        ax1.tick_params(axis='both', which='both', direction='out')
        ax1.minorticks_on()
    else:
        ax1.spines["top"].set_visible(False)
        ax1.spines["bottom"].set_visible(False)
        ax1.spines["left"].set_visible(False)
        ax1.xaxis.set_ticks_position('none')
        ax1.yaxis.set_ticks_position('right')
        ax1.tick_params(axis='both', which='both', direction='out')
        ax1.minorticks_on()
    return ax1

def confax_right(ax1):

    """Configure a sub-figure that appears to the right of another
    sub-figure, and shares the y-axis.

    """

    ax1.spines["top"].set_visible(False)
    ax1.spines["left"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    plt.setp(ax1.get_yticklabels(), visible=False)
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('none')
    ax1.tick_params(axis='both', which='both', direction='out')
    ax1.minorticks_on()
    return ax1

def confax_top(ax1):

    """Configure a sub-figure that appears above another sub-figure, and
    shares the x-axis.

    """
    ax1.spines["top"].set_visible(False)
    ax1.spines["bottom"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    plt.setp(ax1.get_xticklabels(), visible=False)
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('left')
    ax1.tick_params(axis='both', which='both', direction='out')
    ax1.minorticks_on()
    return ax1

def confax_none(ax1):

    """Configure a sub-figure with no visible axes."""

    ax1.spines["top"].set_visible(False)
    ax1.spines["bottom"].set_visible(False)
    ax1.spines["left"].set_visible(False)
    ax1.spines["right"].set_visible(False)
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax1.get_yticklabels(), visible=False)
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    ax1.tick_params(axis='both', which='both', direction='out')
    return ax1

def canvas(size='normal', width=None, height=None, aspect=None):

    """Create a figure with various standard sizes and aspect ratios."""

    # figsize is w,h tuple in inches

    # \usepackage{layouts}
    # textwidth:  \printinunitsof{in}\prntlen{\textwidth}
    # linewidth:  \printinunitsof{in}\prntlen{\linewidth}
    # textheight: \printinunitsof{in}\prntlen{\textheight}
    #
    # textwidth:  6.75133in
    # linewidth:  3.27563in
    # textheight: 9.25182in

    text_width = 6.7
    text_height = 9.2

    if size == 'normal':
        # Full width at 16:9 aspect ratio.
        xsize = text_width
        ysize = xsize / (16/9)
    elif size == 'twocol':
        # Half width at 4:3 aspect ratio.
        xsize = 0.96 * (text_width / 2)
        ysize = xsize / (4/3)
    elif size == 'full':
        # Full page figure.
        xsize = text_width
        ysize = 0.95 * text_height
    elif size == 'narrow':
        # Full width at 16:6 aspect ratio.
        xsize = text_width
        ysize = xsize / (16/6)
    elif size == 'square':
        # Full width at 1:1 aspect ratio.
        xsize = text_width
        ysize = xsize
    else:
        raise ValueError('Unrecognised image size.')

    if width is not None:
        xsize = width

    if height is not None:
        ysize = height

    if aspect is not None:
        ysize = xsize / aspect

    return plt.figure(figsize=(xsize, ysize))

def save(fig, filespec, tight=True):

    """Save the figure with a standard DPI, and optionally with or without
    tight_layout() being called.

    """

    if tight:
        fig.tight_layout()
    fig.savefig(filespec, dpi=300, bbox_inches='tight')
    return True

########################################################################
