#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  FFT.PY
#
#    Steven Hale
#    2016 July 19
#    Birmingham, UK
#

"""Tests for the FFT module."""

import pytest
import numpy
import core.fft

########################################################################
########################################################################
########################################################################

class TestSignal():

    """Tests creation of signal data."""

    @staticmethod
    def test_signal():
        """Test signal generation."""
        amplitude = 1 # arb. unit.
        mean = 0 # arb. unit.
        frequency = 10 # Hz
        rate = 1000 # Hz
        duration = 1 # seconds
        data = core.fft.generate_signal(amplitude, mean, frequency, rate, duration)
        assert numpy.mean(data['signal']) == pytest.approx(mean)
        assert numpy.std(data['signal']) == pytest.approx(1/(2**(0.5)))

class TestNoise():

    """Tests creation of random noise."""

    amplitude = [1, 2, 3, 1, 2, 3]
    mean = [0, 0, 0, 2, 4, 8]

    @staticmethod
    @pytest.mark.parametrize("amplitude,mean", list(zip(amplitude, mean)))
    def test_random_noise(amplitude, mean):
        """Test various random noise parameters."""
        points = 1000
        noise = core.fft.generate_random_noise(amplitude, mean, points)
        assert numpy.mean(noise) == pytest.approx(mean)
        assert numpy.max(noise) == pytest.approx(mean+amplitude, abs=0.15)
        assert numpy.min(noise) == pytest.approx(mean-amplitude, abs=0.15)

    @staticmethod
    @pytest.mark.parametrize("sigma,mean", list(zip(amplitude, mean)))
    def test_gaussian_noise(sigma, mean):
        """Test various gaussian noise parameters."""
        points = 1000
        noise = core.fft.generate_gaussian_noise(sigma, mean, points)
        assert numpy.std(noise) == pytest.approx(sigma, abs=0.2)
        assert numpy.mean(noise) == pytest.approx(mean, abs=0.27)

########################################################################

class TestSpectrum():

    """Tests spectrum scaling."""

    def setup_method(self):
        """Generate test signal with additive white Gaussian noise (AWGN)."""
        # Generate signal.
        amplitude = 1 # arb. unit.
        mean = 0 # arb. unit.
        frequency = 10 # Hz
        rate = 1000 # Hz
        duration = 1 # seconds
        data = core.fft.generate_signal(amplitude, mean, frequency, rate, duration)
        self.dt = data['time'][1] - data['time'][0]

        # Generate white gaussian noise.
        sigma = 1 # RMS amplitude, arb. unit.
        mean = 0 # arb. unit.
        noise = core.fft.generate_gaussian_noise(1, 0, len(data['signal']))

        # Combine signal and noise.
        data['signal'] += noise
        self.data = data


    def test_energy(self):
        """
        Tests energy levels in timeseries and spectrum.

        If the spectrum is correctly scaled according to Parseval's
        theorem, then the energy in the time domain should be equal
        to the energy in the frequency domain, to within numerical
        precision.

        """

        energy_time = core.fft.calc_energy_in_time_domain(self.data['signal'], self.dt)
        energy_frequency = core.fft.calc_energy_in_frequency_domain(self.data['signal'], self.dt)
        assert energy_time == pytest.approx(energy_frequency, rel=1e-2)


    def test_nonzero_offset(self):
        """
        Tests mean subtraction, to create a timeseries centered on zero
        mean.  If the timeseries has a non-zero mean then bin zero of
        the spectral density will be non-zero.  If the timeseries has
        zero mean, then bin zero of the spectral density will be zero.

        """

        # Offset the signal with a non-zero mean.
        self.data['signal'] += 1

        # The first bin (DC) should be non-zero.
        sd = core.fft.calc_fft(self.data['signal'], self.dt, zeromean=False)
        assert sd['spectrum'][0] != 0
        # When the mean is subtracted the first bin should be zero.
        sd = core.fft.calc_fft(self.data['signal'], self.dt, zeromean=True)
        assert sd['spectrum'][0] == pytest.approx(0)


    power = [False, True, False, True]
    parseval = [False, True, True, False]

    @pytest.mark.parametrize("power,parseval", list(zip(power, parseval)))
    def test_rms(self, power, parseval):
        """Test the spectrum RMS equals the time domain RMS."""
        sd = core.fft.calc_fft(self.data['signal'], self.dt, power=power, parseval=parseval)
        rms = core.fft.spectrum_to_rms(sd['spectrum'], self.dt, power=power, parseval=parseval)
        assert numpy.std(self.data['signal']) == pytest.approx(rms, rel=1e-2)


    @pytest.mark.parametrize("power,parseval", list(zip(power, parseval)))
    def test_spectrum_to_timeseries(self, power, parseval):
        """Test inverse FFT from spectrum to timeseries."""
        sd = core.fft.calc_fft(self.data['signal'], self.dt, power=power, parseval=parseval)
        data = core.fft.spectrum_to_timeseries(sd['spectrum'], self.dt, power=power, parseval=parseval)
        dt = data['time'][1]-data['time'][0]
        assert dt == self.dt
        assert numpy.std(self.data['signal']) == pytest.approx(numpy.std(data['data']), abs=1e-2)
        assert numpy.mean(self.data['signal']) == pytest.approx(numpy.mean(data['data']), abs=1e-1)

########################################################################
