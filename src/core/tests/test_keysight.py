#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  KEYSIGHT.PY
#
#    Steven Hale
#    2016 February 10
#    Birmingham, UK
#

"""Tests for the keysight module."""

import core.keysight

# pylint: disable=attribute-defined-outside-init

########################################################################
########################################################################
########################################################################

class TestOutput():

    """Test we can acquire the expected data from a known file."""

    def setup_method(self):
        """Open a known test file."""
        self.data = core.keysight.read('../data/20170903-morning/scope_0.csv')

    def test_time(self):
        """Checked the results from the time column."""
        assert self.data['time'][0] == -5
        assert self.data['time'][-1] == +4.99984

    def test_ch1(self):
        """Checked the results from the ch1 column."""
        assert self.data['ch1'][0] == +677.7890E-03
        assert self.data['ch1'][-1] == +680.4020E-03

########################################################################
