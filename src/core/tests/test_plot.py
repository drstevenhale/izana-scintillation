#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  AGILENT86142B.PY
#
#    Steven Hale
#    2015 August 13
#    Birmingham, UK
#

"""Tests the plot configuration functions."""

import pytest
import matplotlib as mpl
import core.plot

# pylint: disable=attribute-defined-outside-init

########################################################################
########################################################################
########################################################################

class TestCanvas():

    """Tests creation of various size figures."""

    @staticmethod
    def test_normal():
        """Test normal size figure."""
        fig = core.plot.canvas('normal')
        assert isinstance(fig, mpl.figure.Figure)

    @staticmethod
    def test_twocol():
        """Test two-column figure."""
        fig = core.plot.canvas('twocol')
        assert isinstance(fig, mpl.figure.Figure)

    @staticmethod
    def test_full():
        """Test full page figure."""
        fig = core.plot.canvas('full')
        assert isinstance(fig, mpl.figure.Figure)

    @staticmethod
    def test_narrow():
        """Test narrow figure."""
        fig = core.plot.canvas('narrow')
        assert isinstance(fig, mpl.figure.Figure)

    @staticmethod
    def test_square():
        """Test square figure."""
        fig = core.plot.canvas('square')
        assert isinstance(fig, mpl.figure.Figure)

    @staticmethod
    def test_unknown():
        """Test response to unknown size figure."""
        with pytest.raises(ValueError):
            core.plot.canvas('foo')

    @staticmethod
    def test_custom():
        """Test custom size figure."""
        fig = core.plot.canvas(width=10, height=5, aspect=16/9)
        assert isinstance(fig, mpl.figure.Figure)
        size = fig.get_size_inches()
        assert size[0] == 10
        assert size[1] == 10 / (16/9)

    @staticmethod
    def test_save():
        """Test saving figure."""
        fig = core.plot.canvas('normal')
        core.plot.save(fig, '/tmp/test.pdf', tight=False)

    @staticmethod
    def test_save_tight():
        """Test saving figure with tight layout."""
        fig = core.plot.canvas('normal')
        core.plot.save(fig, '/tmp/test.pdf')

########################################################################

class TestFigures():

    """Tests creation of different figure configurations."""

    def setup_method(self):
        """Start a normal size figure with one subplot."""
        self.fig = core.plot.canvas('normal')
        self.ax1 = self.fig.add_subplot(111)

    @staticmethod
    def test_latex():
        """Activate LaTeX for text."""
        core.plot.set_latex()
        assert mpl.rcParams['text.usetex']
        assert mpl.rcParams['font.family'] == ['serif']

    @staticmethod
    def test_fonts():
        """Set font size for all text."""
        size = 10
        core.plot.set_fonts(font_size=size)
        assert mpl.rcParams['font.size'] == size
        assert mpl.rcParams['axes.titlesize'] == size
        assert mpl.rcParams['axes.labelsize'] == size
        assert mpl.rcParams['xtick.labelsize'] == size
        assert mpl.rcParams['ytick.labelsize'] == size
        assert mpl.rcParams['legend.fontsize'] == size
        assert mpl.rcParams['figure.titlesize'] == size

    def test_left_axis(self):
        """Test config for a left-hand axis."""
        core.plot.confax(self.ax1, side='left')
        assert self.ax1.spines['top'].get_visible() is False
        assert self.ax1.spines['right'].get_visible() is False
        assert self.ax1.xaxis.get_ticks_position() == 'bottom'
        assert self.ax1.yaxis.get_ticks_position() == 'left'

        for tick in self.ax1.xaxis.get_major_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.xaxis.get_minor_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

        for tick in self.ax1.yaxis.get_major_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.yaxis.get_minor_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

    def test_right_axis(self):
        """Test config for a right-hand axis."""
        core.plot.confax(self.ax1, side='right')
        assert self.ax1.spines['top'].get_visible() is False
        assert self.ax1.spines['bottom'].get_visible() is False
        assert self.ax1.spines['left'].get_visible() is False
        assert self.ax1.spines['right'].get_visible()
        assert self.ax1.xaxis.get_ticks_position() == 'unknown'
        assert self.ax1.yaxis.get_ticks_position() == 'right'

        for tick in self.ax1.xaxis.get_major_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.xaxis.get_minor_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

        for tick in self.ax1.yaxis.get_major_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible()
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.yaxis.get_minor_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible()
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

    def test_right_figure(self):
        """Test config for a figure that appears to the right of another
        figure, with shared y-axis."""
        core.plot.confax_right(self.ax1)
        assert self.ax1.spines['top'].get_visible() is False
        assert self.ax1.spines['bottom'].get_visible()
        assert self.ax1.spines['left'].get_visible() is False
        assert self.ax1.spines['right'].get_visible() is False
        assert self.ax1.xaxis.get_ticks_position() == 'bottom'
        assert self.ax1.yaxis.get_ticks_position() == 'unknown'

        for tick in self.ax1.xaxis.get_major_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.xaxis.get_minor_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

        for tick in self.ax1.yaxis.get_major_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.yaxis.get_minor_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

    def test_top_figure(self):
        """Test config for a figure that appears above another figure, with
        shared x-axis."""
        core.plot.confax_top(self.ax1)
        assert self.ax1.spines['top'].get_visible() is False
        assert self.ax1.spines['bottom'].get_visible() is False
        assert self.ax1.spines['left'].get_visible()
        assert self.ax1.spines['right'].get_visible() is False
        assert self.ax1.xaxis.get_ticks_position() == 'unknown'
        assert self.ax1.yaxis.get_ticks_position() == 'left'

        for tick in self.ax1.xaxis.get_major_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.xaxis.get_minor_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

        for tick in self.ax1.yaxis.get_major_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.yaxis.get_minor_ticks():
            assert tick.tick1line.get_visible()
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

    def test_noaxes_figure(self):
        """Test config for a figure with all axes turned off."""
        core.plot.confax_none(self.ax1)
        assert self.ax1.spines['top'].get_visible() is False
        assert self.ax1.spines['bottom'].get_visible() is False
        assert self.ax1.spines['left'].get_visible() is False
        assert self.ax1.spines['right'].get_visible() is False
        assert self.ax1.xaxis.get_ticks_position() == 'unknown'
        assert self.ax1.yaxis.get_ticks_position() == 'unknown'

        for tick in self.ax1.xaxis.get_major_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.xaxis.get_minor_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

        for tick in self.ax1.yaxis.get_major_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'
        for tick in self.ax1.yaxis.get_minor_ticks():
            assert tick.tick1line.get_visible() is False
            assert tick.tick2line.get_visible() is False
            assert tick.gridline.get_visible() is False
            assert tick.get_tickdir() == 'out'

########################################################################
