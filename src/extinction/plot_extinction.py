#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  PLOT_EXTINCTION
#
#    Steven Hale
#    2017 September 5
#    Izana, Tenerife, Spain
#

"""Plot extinction levels at Izana."""

import sys
import datetime
import logging
import numpy

import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

import core.plot as plot

########################################################################
########################################################################
########################################################################

def resize(aDT, aData, n, sType = 'median'):

    # Split 1D array into 2D array with slices of n.
    aDT   = numpy.resize(aDT, [int(len(aDT) / n), n])
    aDT   = aDT[:,0]
    aData = numpy.resize(aData, [int(len(aData) / n), n])

    # Find the number of non-nan values in each slice.
    n = (~numpy.isnan(aData)).sum(axis = 1)

    if   sType is 'mean':
        tmp = aData
        aData  = numpy.nanmean(tmp, axis = 1)
        aSigma = numpy.nanstd(tmp, axis = 1)

    elif sType is 'median':
        tmp = aData
        aData  = numpy.nanmedian(tmp, axis = 1)
        # Find the interquartile range.
        q75, q25 = numpy.nanpercentile(tmp, [75, 25], axis = 1)
        iqr = q75 - q25
        # One standard deviation is 0.741 of the interquartile range.
        aSigma = 0.741 * iqr

    else:
        raise ValueError('Unknown resize type.')

    # Now divide by root-n to get the standard error.

    aSigma = aSigma / numpy.sqrt(n)

    return aDT, aData, aSigma

########################################################################

def main():

    sFilespec = '../data/extinction/izana_extinction.dat'

    aData = numpy.genfromtxt(sFilespec, comments = '#', delimiter = ' ', names = True, dtype = None)

    aDT = aData['dt']
    aDT = numpy.array([datetime.datetime.strptime(dt.decode('UTF-8'), '%Y-%m-%dT%H:%M:%S.%f') for dt in aDT])

    n = 14
    sType = 'median'

    aDTsm, aSMorning,   aSMorningSigma   = resize(aDT, aData['sm'], n, sType = sType)
    aDTsa, aSAfternoon, aSAfternoonSigma = resize(aDT, aData['sa'], n, sType = sType)

    plot.set_latex()
    plot.set_fonts()

    fig = plot.canvas(size='normal', aspect=4/3)

    minor_locator = AutoMinorLocator(5)

    ax1 = fig.add_subplot(311)
    plot.confax(ax1)
    ax1.xaxis.set_minor_locator(minor_locator)
    plt.setp(ax1.get_xticklabels(), visible=False)
    ax1.locator_params(axis='y', nbins=7)
    ax2 = fig.add_subplot(312, sharex = ax1, sharey = ax1)
    plot.confax(ax2)
    ax2.xaxis.set_minor_locator(minor_locator)
    ax3 = fig.add_subplot(325)
    plot.confax(ax3)
    ax4 = fig.add_subplot(326, sharex = ax3, sharey = ax3)
    plot.confax_right(ax4)

    pos = ax2.get_position().bounds
    xoffset = 0.0
    yoffset = 0.005
    newpos = [pos[0]+xoffset, pos[1]+yoffset, pos[2]+xoffset, pos[3]+yoffset]
    ax2.set_position(newpos)

    xoffset = 0.01
    yoffset = -0.048

    pos = ax3.get_position().bounds
    newpos = [pos[0]+xoffset, pos[1]+yoffset, pos[2]+xoffset, pos[3]+yoffset]
    ax3.set_position(newpos)

    pos = ax4.get_position().bounds
    newpos = [pos[0]-xoffset, pos[1]+yoffset, pos[2]-xoffset, pos[3]+yoffset]
    ax4.set_position(newpos)

    #fig.subplots_adjust(hspace=0.01)

    #ax1.set_xlim([aDT[0], aDT[-1]])
    #ax2.set_xlim([aDT[0], aDT[-1]])
    ax1.set_xlim(datetime.datetime(1975, 1, 1), datetime.datetime(2020, 1, 1))
    ax2.set_xlim(datetime.datetime(1975, 1, 1), datetime.datetime(2020, 1, 1))

    nBins = 61
    nRange=[0.0,0.15]
    ax1.set_ylim([0.0, 0.6])
    ax2.set_ylim([0.0, 0.6])
    ax3.set_xlim(nRange)
    ax4.set_xlim(nRange)

    ax2.set_xlabel('Date')
    ax1.set_ylabel('Extinction')
    ax2.set_ylabel('Extinction')

    ax3.set_xlabel('Extinction coefficient')
    ax4.set_xlabel('Extinction coefficient')
    ax3.set_ylabel('Prob. density')
    #ax4.set_ylabel('Prob. density')

    ax3.locator_params(nbins=5, axis='y')

    nSigma = 3

    ax1.fill_between(aDTsm, aSMorning - nSigma * aSMorningSigma, aSMorning + nSigma * aSMorningSigma, facecolor='k', alpha=0.3,
                     edgecolor=None, linewidth=0.0)
    ax1.scatter(aDTsm, aSMorning, label=None, s=1, color='k', alpha=0.7)

    ax2.fill_between(aDTsa, aSAfternoon - nSigma * aSAfternoonSigma, aSAfternoon + nSigma * aSAfternoonSigma, facecolor='k', alpha=0.3,
                     edgecolor=None, linewidth=0.0)
    ax2.scatter(aDTsa, aSAfternoon, label='Scatter', s=1, color='k', alpha=0.7)

    aSMorning = aData['sm']
    aSMorning = aSMorning[numpy.isfinite(aSMorning)]
    aSAfternoon = aData['sa']
    aSAfternoon = aSAfternoon[numpy.isfinite(aSAfternoon)]

    aBins, nStep = numpy.linspace(nRange[0], nRange[1], num=nBins, endpoint=True, retstep=True)
    aN, aBins, patches = ax3.hist(aSMorning, bins=aBins, histtype='bar', color='k', edgecolor='k', alpha=0.5, density=True)
    aN, aBins, patches = ax4.hist(aSAfternoon, bins=aBins, histtype='bar', color='k', edgecolor='k', alpha=0.5, density=True)

    ax1.text(0.05, 0.8, 'Morning',
             transform=ax1.transAxes,
             color='k', fontsize=8)

    ax2.text(0.05, 0.8, 'Afternoon',
             transform=ax2.transAxes,
             color='k', fontsize=8)

    ax3.text(0.7, 0.3, 'Morning',
             transform=ax3.transAxes,
             color='k', fontsize=8)

    ax4.text(0.65, 0.3, 'Afternoon',
             transform=ax4.transAxes,
             color='k', fontsize=8)

    plot.save(fig, '../paper/izana_extinction.pdf')
    plt.close()

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':

    #logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s %(message)s', level=logging.DEBUG)
    logging.basicConfig(format='%(asctime)s %(module)s %(levelname)s %(message)s', level=logging.INFO)

    main()

########################################################################
