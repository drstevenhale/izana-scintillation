#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  PLOT_DUST
#
#    Steven Hale
#    2017 September 5
#    Izana, Tenerife, Spain
#

"""Plot dust levels at Izana."""

import sys
import numpy
import datetime

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.colors as colors
from   matplotlib.colors import LinearSegmentedColormap
from   matplotlib.ticker import FormatStrFormatter

import core.plot as plot

########################################################################
########################################################################
########################################################################

def plot_dust(ax):

    aData = numpy.loadtxt('../data/dust/izana_dust.dat',
                          delimiter=',',
                          dtype={'names': ('datetime', 'dust'),
                                 'formats': ('|S35', numpy.float)})

    aDT = []
    for sDate in aData['datetime']:
        try:
            dt = datetime.datetime.strptime(sDate.decode('utf-8'), '%d.%m.%Y %H:%M:%S.%f')
        except:
            dt = datetime.datetime.strptime(sDate.decode('utf-8'), '%d.%m.%Y %H:%M:%S')
        aDT.append(dt)

    aDT = numpy.array(aDT)

    dt_start  = datetime.datetime(2009, 1, 1)
    dt_finish = datetime.datetime(2020, 1, 1)
    ax.set_xlim(dt_start, dt_finish)
    ax.set_ylim(0,0.11)
    ax.set_ylabel('$\mathrm{PLA\,m^{-3}}$')

    ax.plot(aDT, aData['dust'], color='k', linewidth=0.2)

    nLevels = 5
    myColors = [(0, 1, 0), (1, 1, 0), (1, 0, 0)]  # G -> Y -> R

    cmap_name = 'myColors'
    cm = LinearSegmentedColormap.from_list(cmap_name, myColors, N=nLevels)
    plt.register_cmap(cmap=cm)
    cm = plt.get_cmap('myColors')
    cNorm  = colors.Normalize(vmin=0, vmax=nLevels-1)
    scalarMap = mpl.cm.ScalarMappable(norm=cNorm, cmap=cm)

    alpha = 0.25

    ax.axhspan(0,      0.0025, facecolor = scalarMap.to_rgba(0), edgecolor=None, alpha=alpha)
    ax.axhspan(0.0025, 0.0100, facecolor = scalarMap.to_rgba(1), edgecolor=None, alpha=alpha)
    ax.axhspan(0.0100, 0.0175, facecolor = scalarMap.to_rgba(2), edgecolor=None, alpha=alpha)
    ax.axhspan(0.0175, 0.0250, facecolor = scalarMap.to_rgba(3), edgecolor=None, alpha=alpha)
    ax.axhspan(0.0250, 1.0000, facecolor = scalarMap.to_rgba(4), edgecolor=None, alpha=alpha)

def resize(dt, data, n):

    # Split 1D array into 2D array with slices of n.
    dt   = numpy.resize(dt, [int(len(dt) / n), n])
    dt   = dt[:,0]
    data = numpy.resize(data, [int(len(data) / n), n])

    # Find the number of non-nan values in each slice.
    n = (~numpy.isnan(data)).sum(axis = 1)

    extinction  = numpy.nanmedian(data, axis = 1)
    # Find the interquartile range.
    q75, q25 = numpy.nanpercentile(data, [75, 25], axis = 1)
    iqr = q75 - q25
    # One standard deviation is 0.741 of the interquartile range.
    sigma = 0.741 * iqr

    # Now divide by root-n to get the standard error.

    sigma = sigma / numpy.sqrt(n)

    return dt, extinction, sigma

def plot_extinction(ax):

    aData = numpy.genfromtxt('../data/extinction/izana_extinction.dat',
                             comments = '#',
                             delimiter = ' ',
                             names = True,
                             dtype = None)

    aDT = aData['dt']
    aDT = numpy.array([datetime.datetime.strptime(dt.decode('UTF-8'), '%Y-%m-%dT%H:%M:%S.%f') for dt in aDT])

    n = 7

    data = numpy.nanmean([[aData['sm'], aData['sa']]], axis=1)
    data = data[0,]

    aDTsm, aSMorning, aSMorningSigma = resize(aDT, data, n)

    nSigma = 3

    ax.fill_between(aDTsm, aSMorning - nSigma * aSMorningSigma, aSMorning + nSigma * aSMorningSigma, facecolor='k', alpha=0.3,
                     edgecolor=None, linewidth=0.0)
    ax.scatter(aDTsm, aSMorning, label=None, s=1, color='k', alpha=0.7)

    ax.set_xlabel('Date')
    ax.set_ylabel('mag/airmass')

    ax.set_ylim([0.0, 0.4])

    years = mdates.YearLocator()   # every year
    months = mdates.MonthLocator() # every month
    years_fmt = mdates.DateFormatter('%Y')

    ax.xaxis.set_major_locator(years)
    ax.xaxis.set_major_formatter(years_fmt)
    ax.xaxis.set_minor_locator(months)


def main(argv):

    plot.set_latex()
    plot.set_fonts()

    # Create a canvas.
    fig = plot.canvas('normal')
    ax1 = fig.add_subplot(211)
    plot.confax_top(ax1)
    ax2 = fig.add_subplot(212, sharex = ax1)
    plot.confax(ax2)

    plot_dust(ax1)
    plot_extinction(ax2)

    plot.save(fig, '../paper/izana_dust.pdf')
    plt.close()

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main(sys.argv)

########################################################################
