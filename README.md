# Atmospheric scintillation at Observatorio del Teide, Iz̃ana, Tenerife.

[![pipeline status](https://gitlab.com/drstevenhale/izana-scintillation/badges/dev/pipeline.svg)](https://gitlab.com/drstevenhale/izana-scintillation/commits/dev)
[![coverage report](https://gitlab.com/drstevenhale/izana-scintillation/badges/dev/coverage.svg)](https://gitlab.com/drstevenhale/izana-scintillation/commits/dev)
[![arXiv](https://img.shields.io/badge/astro--ph.IM-%20%09arXiv%3A1912.12237-B31B1B)](https://arxiv.org/abs/1912.12237)
[![eData](https://img.shields.io/badge/UBIRA%20eData-doi%3Aedata.bham.00000412-605270)](https://doi.org/10.25500/edata.bham.00000412)
[![IOP](https://img.shields.io/badge/IOP-doi%3A10.1088%2F1538--3873%2Fab6753-006eb2)](https://doi.org/10.1088/1538-3873/ab6753)

A repository containing the paper, data, and code for the publication
"[Measurement of Atmospheric Scintillation during a Period of
Saharan Dust (Calima) at Observatorio del Teide, Iz̃ana, Tenerife, and
the Impact on Photometric Exposure
Times](https://doi.org/10.1088/1538-3873/ab6753)" published by
[Publications of the Astronomical Society of the
Pacific](https://iopscience.iop.org/journal/1538-3873).

The accepted version of the paper is availble from
[arXiv](https://arxiv.org/abs/1912.12237).  The data are also
available via the University of Birmingham [eData
archive](https://doi.org/10.25500/edata.bham.00000412).

# Citation

If you wish to make use of these data, please cite both the paper and
the data repository using the following BibTeX:

```
@ARTICLE{2020PASP..132c4501H,
       author = {{Hale}, S.~J. and {Chaplin}, W.~J. and {Davies}, G.~R. and
         {Elsworth}, Y.~P. and {Howe}, R. and {Pall{\'e}}, P.~L.},
        title = "{Measurement of Atmospheric Scintillation during a Period of Saharan Dust (Calima) at Observatorio del Teide, I{\~z}ana, Tenerife, and the Impact on Photometric Exposure Times}",
      journal = {\pasp},
     keywords = {atmospheric effects},
         year = "2020",
        month = "Mar",
       volume = {132},
       number = {1009},
        pages = {034501},
          doi = {10.1088/1538-3873/ab6753},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2020PASP..132c4501H},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```

```
@misc{edata412,
      title = {{Measurement of atmospheric scintillation at Observatorio del Teide, I{\~z}ana, Tenerife}},
     author = {{Hale}, S.~J},
  publisher = {Birmingham Solar Oscillations Network},
       year = {2019},
      month = dec,
   keywords = {atmospheric scintillation, saharan dust, calima},
        doi = {10.25500/edata.bham.00000412},
        url = {https://edata.bham.ac.uk/412/}
}
```
