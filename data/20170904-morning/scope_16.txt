ANALOG
Ch 1 Scale 5mV/, Pos 590.000mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 620.000mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 1.000s/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Average - Full Screen(1), Cur 596.86mV
Std Dev(1), Cur 1.114mV
RMS - Full Screen(1), Cur 597mV

