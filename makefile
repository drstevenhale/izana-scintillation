#
#  MAKEFILE
#
#    Steven Hale
#    2019 June 17
#    Birmingham, UK
#

################################################################

.PHONY: all help src paper clean

all: src paper

help :
	@echo "Targets:"
	@echo "all        Make src and paper."
	@echo "src        Build the source code."
	@echo "paper      Build the paper."
	@echo "clean      Clean up."

clean :
	git clean -dxf

src:
	make --directory=src build
	make --directory=src test

paper:
	make --directory=paper all

################################################################

# Run in --silent mode unless the user sets VERBOSE=1 on the                    
# command-line.                                                                 

ifndef VERBOSE
.SILENT:
endif
