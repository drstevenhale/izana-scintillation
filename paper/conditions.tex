% -*- coding: utf-8 -*-
%
% CONDITIONS.TEX
%
%   Steven Hale
%   2019 July 29
%   Birmingham, UK
%
% Atmospheric conditions.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Atmospheric conditions}
\label{conditions}

\begin{figure*}[t]
  \centering
  \includegraphics[scale=1.0]{izana_extinction}
  \caption{Extinction coefficients and statistical distribution from
    Iza{\~n}a, Tenerife, estimated with the Mark-I BiSON instrument
    using the technique described by~\citet{1538-3881-154-3-89}.
    Morning and afternoon extinction values are estimated separately,
    shown in the top and middle panel.  Each dot represents the median
    value over 14~days.  The grey shading represents $\pm3$ times the
    standard-error on each median value.  The large increase in
    extinction during the period between 1991 and 1994 was caused by
    the eruption of the Mount Pinatubo volcano in the Philippines.
    The bottom two panels show the statistical distribution of
    extinction, which is Gaussian with a long-tail caused by the
    effects of Calima.}
  \label{fig:izana_extinction}
\end{figure*}

In order to interpret the changes in atmospheric scintillation noise
characteristics it is essential to also estimate the overall
atmospheric conditions. This is particularly relevant to Iza{\~n}a,
where placing results on scintillation in context requires an
understanding of the impact of the Calima.  The atmospheric extinction
coefficient was determined each day using the technique described
by~\citet{1538-3881-154-3-89}.  In brief, this involved logging the
changing solar intensity measured throughout the day by our BiSON
instrument, and fitting this for both morning and afternoon periods
against the airmass calculated from the known solar zenith angle,
%
\begin{equation}%\label{eq:atmos_ext}
  \ln(I/I_0) = -\tau A ~,
\end{equation}
%
where $I$ is the direct-Sun radiance, $I_0$ the maximum intensity
measured on a given day, and $A$ the airmass.  The gradient of the
fit, $\tau$, is a measure of the column atmospheric aerosol optical
depth (AOD) per unit airmass, calibrated in terms of
magnitudes~per~airmass.  Figure~\ref{fig:izana_extinction} shows the
estimated atmospheric extinction coefficients determined from the
archive of data from the Mark-I BiSON instrument at Iza{\~n}a.  The
typical extinction coefficient at Iza{\~n}a on a clear day is
about~\num{0.05}~magnitudes~per~airmass.  During mineral dust events,
often between June and October, extinction can rise to values
between~\num{0.1} and~\num{0.8}~magnitudes~per~airmass.

\begin{figure*}
  \centering
  \includegraphics[scale=1.0]{izana_dust}
  \caption{Top: Dust data obtained with the~\citet{dust} at Iza{\~n}a,
    an AIP facility jointly operated by AIP and IAC.  The coloured
    bands indicate the qualitative limits described in
    Table~\ref{table:dust_limits}.  Towards the end of 2018 a fault
    was discovered with the wiring of the sensor, which following the
    repair resulted in a cleaner signal and calibration change.
    Bottom: Average daily extinction coefficients at Iza{\~n}a
    estimated with the BiSON instrument over the same time period.
    Each dot represents the median value over 7~days.  The grey
    shading represents $\pm3$ times the standard-error on each median
    value.  The Spearman rank-order correlation coefficient for the
    two datasets is~\num{0.5} with an infinitesimally small
    false-alarm probability.}
  \label{fig:dust}
\end{figure*}

A real-time estimate of ground-level dust at Iza{\~n}a is provided by
the Stellar Activity (STELLA) robotic telescopes~\citeyearpar{dust}, a
Leibniz Institute for Astrophysics Potsdam (AIP) facility. STELLA make
available, amongst other parameters, measurements from a VisGuard~2
In-situ Visibility Monitor manufactured by~\citet{14289E/4}.  The
VisGuard is a photometer that measures the intensity of scattered
light from an air sample drawn into the instrument by a fan.  The
output is a measure of Polystyrol-Latex-Aerosols (PLA) per cubic
metre, and when the instrument is installed in its intended use case
monitoring visibility in vehicle tunnels, this value can be converted
to an extinction coefficient.  When used in open-air observations, the
factory calibration is uncertain.  The IAC have therefore defined some
threshold levels for what is considered to be low and high, and these
are detailed in Table~\ref{table:dust_limits}.  The archive of data
from the VisGuard is shown in Figure~\ref{fig:dust} with coloured
banding to indicate the IAC thresholds.  The period of unusually high
values towards the end of 2013 results from a sensor glitch, and at
the end of 2018 a repair to the wiring of the sensor has reduced the
noise level and resulted in a change of calibration.

\begin{table}
    \caption{Qualitative limits used at the IAC in calibration of
      ground-level dust measurements from the~\citet{dust}.}
    \label{table:dust_limits}
    \footnotesize
    \centering
    \begin{tabular}{l l c c}
      \toprule

      \multicolumn{2}{c}{\makecell[cb]{Qualitative Value}} &
      \multicolumn{2}{c}{\makecell[cb]{Range ($\mathrm{PLA\,m^{-3}}$)}}
      \\
      \cmidrule(lr){1-2}\cmidrule(lr){3-4}
      Spanish & English & Min & Max\\
      
      \midrule

      Poca          & Little         &              & \num{<0.0025}\\
      Media         & Medium         & \num{0.0025} & \num{<0.0100}\\
      Bastante      & Quite a lot    & \num{0.0100} & \num{<0.0175}\\
      Mucha         & A lot          & \num{0.0175} & \num{<0.0250}\\
      Fuera limites & Outside limits & \num{0.0250} &\\

      \bottomrule

    \end{tabular}
\end{table}

The two results do not necessarily show perfect correlation, since the
BiSON atmospheric extinction measurement is estimating the column
aerosol optical depth through the entire atmosphere, and the VisGuard
is estimating the amount of dust at only ground level.  In general,
when the extinction is high the PLA tends to be high, and the Spearman
rank-order correlation coefficient is~\num{0.5} with an
infinitesimally small false-alarm probability.  When considered
together the two measurements provide a good qualitative judgement of
atmospheric conditions, and this allows us to better understand the
following frequency-dependent scintillation measurements, which show
significant variation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
