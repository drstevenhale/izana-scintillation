% -*- coding: utf-8 -*-
%
% DISCUSSION.TEX
%
%   Steven Hale
%   2017 August 6
%   Birmingham, UK
%
% Atmospheric scintillation.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Discussion}
\label{discussion}

BiSON spectrophotometers have made use of various optoelectronic
components to achieve computer-controlled wavelength selection via
polarisation switching, with rates varying from~\SI{0.5}{\hertz}
to~\SI{50}{\kilo\hertz}.  Modern BiSON spectrophotometers use Pockels
effect cells switched at~\SI{90.9}{\hertz}.  The data acquisition
subsystem takes~\SI{5}{\milli\second} exposures per polarisation
phase, plus~\SI{0.5}{\milli\second} stabilisation time at each
switching point, making an~\SI{11}{\milli\second} period.  This is
repeated 340~times for~\SI{3.74}{\second}, giving a total exposure
integration of~\SI{3.4}{\second} within each~\SI{4}{\second} sample.
Faster switching has the detrimental effect of reducing the overall
integration time due the need for stabilisation periods at each
switching interval.  Slower switching is less effective at removal of
scintillation noise but reduces the dead time.  In order to determine
the optimum switching rate it is necessary to know the characteristics
of atmospheric scintillation noise.

Many polarisation switching techniques are not capable of reaching
rates as high as~\SI{100}{\hertz}, and so during instrumentation
design it is essential to know the potential impact of atmospheric
scintillation on the overall noise levels, and how this compares with
other noise sources in the system.  Electronic noise, photon shot
noise, and noise from thermal fluctuations each contribute less
than~\SI{1}{\pico\watt\per\sqrthz} to the overall noise level, and by
comparison with the values shown in
Table~\ref{table:noise_characteristics}, we see the system is
dominated by scintillation noise as is expected for ground-based
photometric measurements.

\begin{table}
    \centering
    \caption{Velocity-calibrated white noise level and noise
      equivalent velocity (NEV) for five BiSON spectrophotometers over
      a 2018 summer observing campaign.}
    \label{table:2018_performance}

    \begin{tabular}{l S[table-format=4.1] S[table-format=4.1]}

    \toprule

    Site & {White noise}               & {NEV}\\
         & \si{\mps\squared\per\hertz} & \si{\centi\meter\per\second}~RMS\\

    \midrule

             Sutherland &  4.3 & 23.1\\ 
      \textbf{BiSON:NG} &  9.9 & 35.2\\
               Narrabri & 10.4 & 36.1\\
           Las~Campanas & 13.5 & 41.1\\
                 Mark-I & 61.6 & 87.7\\

    \bottomrule

    \end{tabular}

\end{table}

Throughout 2018, a next generation prototype BiSON spectrophotometer
was commissioned at Iza{\~n}a alongside the Mark-I instrument, sharing
light from the pyramid coelostat~\citep{halephd}.  The aim of
{BiSON:NG} is to miniaturise and simplify the instrumentation as much
as practical, typically though the use of off-the-shelf components,
whilst maintaining performance comparable with the existing network.
The prototype spectrophotometer makes use of an LCD retarder for
wavelength selection, and such devices are much slower to change state
than bespoke Pockels effect cells and drivers.  The prototype
instrument switches polarisation state at~\SI{5}{\hertz}, consisting
of a~\SI{50}{\milli\second} stabilisation period followed
by~\SI{50}{\milli\second} exposure time per polarisation phase, a
total of~\SI{200}{\milli\second} per observation data point.
Table~\ref{table:2018_performance} shows the noise performance for
five BiSON spectrophotometers over a 2018 summer observing campaign.
Mark-I at Iza{\~n}a switches at~\SI{0.5}{\hertz}.  Three instruments
at Las~Campanas in Chile, Narrabri in Australia, and Sutherland in
South Africa, all switch at~\SI{90.9}{\hertz}.  Comparing the faster
switching instruments with the slow-switching Mark-I instrument, we
see reduction in white noise power of~\SIrange{-6.6}{-11.5}{\decibel}
which as we have seen here is of the order expected through reduction
in scintillation noise alone.  The prototype instrument produces
mid-range performance inline with the faster switching instruments,
and is consistent with the scintillation noise profiles and
variability we have shown here.

Having such low noise is essential to allow the detection of very-low
frequency solar p-mode oscillations, which have amplitudes of a
centimetre-per-second or less~\citep{2014MNRAS.439.2025D}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
