% -*- coding: utf-8 -*-
%
% INTRODUCTION.TEX
%
%   Steven Hale
%   2017 August 6
%   Birmingham, UK
%
% Atmospheric scintillation.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

The Birmingham Solar Oscillations Network (BiSON) is a six-site
ground-based network of spectrophotometers observing oscillations of
the Sun~\citep{s11207-015-0810-0}.  For many ground-based photometers,
the dominant noise source is that from atmospheric
scintillation~\citep{2015MNRAS.452.1707O}.  The BiSON
spectrophotometers seek to reduce the effect of atmospheric
scintillation by making use of a multi-wavelength polarisation
switching technique.  The basic measurement is Doppler velocity,
determined from the changes in intensity at two points in the wings of
the potassium absorption line at~\SI{770}{\nano\metre}.  Two short
(\SI{\approx5}{\milli\second}) exposures are taken consecutively and
the ratio, $R$, of intensity between the two wings is determined,
% 
\begin{equation}%\label{eq:ratio}
  R = \frac{I_{\mathrm{b}} - I_{\mathrm{r}}}{I_{\mathrm{b}} + I_{\mathrm{r}}} ~,
\end{equation}
%
where $I_{\mathrm{b}}$ and $I_{\mathrm{r}}$ are the intensities
measured at the blue and red wings of the solar absorption line,
respectively.  The ratio is proportional to the observed line-of-sight
Doppler velocity shift.  Integrating many short exposures and
calculating the ratio in this way allows common intensity fluctuations
to cancel, reducing the effect of atmospheric scintillation noise.

The BiSON node located at Iza{\~n}a on the island of Tenerife, at the
Instituto de Astrof{\'i}sica de Canarias
(IAC)~\citep{2014MNRAS.443.1837R}, suffers additional complications in
terms of visibility and atmospheric noise due to the proximity of the
Western Sahara, just~\SI{100}{\kilo\metre} from the North African
coast.  During the summer months the Canary Islands frequently
experience high concentrations of mineral dust in the atmosphere,
known as Calima, where the Saharan Air Layer passes over causing a
fog-like reduction in visibility and a change in atmospheric
characteristics.  The aim of this paper is to determine the
contribution of atmospheric scintillation noise to the overall noise
budget in BiSON observations, and to investigate the change in
atmospheric noise characteristics with periods of Calima.  There are
several techniques for estimation and approximation of the effect of
scintillation noise -- see, e.g.,
\citet{1967AJ.....72..747Y,1969ApOpt...8..869Y,1997PASP..109..173D,1998PASP..110..610D,2006PASP..118..924K,2012A&A...546A..41K,SHEN2014160,2019MNRAS.489.5098F}.
Here, we take an empirical approach to exploring the temporal
frequency spectrum of the scintillation.  The scintillation noise
measurements presented will be of general interest to astronomers at
Iza{\~n}a, however it should be noted that these day-time results will
be worse than night-time conditions.

In Section~\ref{conditions} we look at two methods of estimating
atmospheric conditions, and in Sections~\ref{data} and~\ref{results}
we present scintillation noise data captured over several days in
September~2017 during a period of mild Calima.  Finally, in
Section~\ref{discussion} we discuss the results both for the impacts
on BiSON spectrophotometer design, and for astronomical observations
more generally.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
